// Include necessary OpenCV header files
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"

// Include necessary C++ headers
#include <iostream>
#include <vector>
#include <ctime>
#include <fstream>
#include <string>

// Include required namespaces
using namespace cv;
using namespace std;

void FAST_Feature_Tracking(Mat img_first, Mat img_second , vector<Point2f>& points_first, vector<Point2f>& points_second, vector<uchar>& track) { 
// This function track the features in the second image and remove all those features which have gone out of the frame 
// or where KLT tracker failed

  // KLT Tracker realted parameters
  vector<float> err;          
  Size winSize=Size(21,21);                                               
  TermCriteria termcrit=TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 30, 0.01);
  calcOpticalFlowPyrLK(img_first, img_second, points_first, points_second, track, err, winSize, 3, termcrit, 0, 0.001);
  //-------------------------------------------------------------------

  int idx_tocheck = 0; // when we remove point indexing will become relative this variable handles the relative indexing
  for( int i=0; i<track.size(); i++)
     {  Point2f tmp_point = points_second.at(i- idx_tocheck);
      if ((track.at(i) == 0)||(tmp_point.x<0)||(tmp_point.y<0))  {
          if((tmp_point.x<0)||(tmp_point.y<0))  {
            track.at(i) = 0;
          }
          points_first.erase (points_first.begin() + (i - idx_tocheck ));
          points_second.erase (points_second.begin() + (i - idx_tocheck));
          idx_tocheck++;
      }

     }

}


void FAST_Feature_Detection(Mat img_first, vector<Point2f>& points_first)  
{  // Extracting fast feature points
  vector<KeyPoint> keypoints_first;
  int tau = 20; // Threshold require by FAST algo
  bool  NonMaxSup = true;
  FAST(img_first, keypoints_first, tau, NonMaxSup);
  // Convert keypoints to point2f format
  KeyPoint::convert(keypoints_first, points_first, vector<int>());
}



// Main Function Begin
int main( int argc, char** argv )	{

  // Create a file to store results
  ofstream result_file;
  result_file.open ("odometry_result.txt");
  
  // Place holders
  Mat first_frame_gray, second_frame_gray;
  Mat Rot_, Trans_; //the final rotation and tranlation vectors containing the 


  // default scale, though this is not correct and we may face scale drift issue
  double scale = 1.00;

  // Path to KITTI data set
  // Using only one feed of stereo camera
  char first_frame_path[500];
  char second_frame_path[500];
  sprintf(first_frame_path, "/mnt/docs/dataset/sequences/00/image_1/%06d.png", 0);
  sprintf(second_frame_path, "/mnt/docs/dataset/sequences/00/image_1/%06d.png", 1);



  //read the first two frames from the dataset
  Mat first_frame_rgb = imread(first_frame_path);
  Mat second_frame_rgb = imread(second_frame_path);


  // Convert to Gray scale images
  cvtColor(first_frame_rgb, first_frame_gray, COLOR_BGR2GRAY);
  cvtColor(second_frame_rgb, second_frame_gray, COLOR_BGR2GRAY);

  
  // storage for keypoint locations
  vector<Point2f> points_first, points_second;        
  // Detect FAST Features in the first image
  FAST_Feature_Detection(first_frame_gray, points_first);        
  // Keep track of lost features
  vector<uchar> keep_track;
  // Tracking features deteted in the first image
  FAST_Feature_Tracking(first_frame_gray,second_frame_gray,points_first,points_second, keep_track);

  // Camera Intrinsic Parameters
  double ff = 718.9;
  cv::Point2d pp(607.2, 185.2);
  
  // Estimate Essential Matrix from point correspondences
  Mat Emat, Rmat, tvec, mask;
  Emat = findEssentialMat(points_second, points_first, ff, pp, RANSAC, 0.999, 1.0, mask);
  recoverPose(Emat, points_second, points_first, Rmat, tvec, ff, pp, mask);



  Mat previous_frame = second_frame_gray;
  Mat current_frame_gray;
  vector<Point2f> previous_frame_features = points_second;
  vector<Point2f> current_frame_features;
  char current_frame_path[100];
  Rot_   = Rmat.clone();
  Trans_ = tvec.clone();
  clock_t start = clock();

  // For plotting traced tragectory
  namedWindow( "Visual", WINDOW_AUTOSIZE );
  namedWindow( "Traced Trajectory", WINDOW_AUTOSIZE );
  Mat traced_trajectory = Mat::zeros(700, 700, CV_8UC3);
 



  for(int frame_id=2; frame_id < 500; frame_id++)	{

  	sprintf(current_frame_path, "/mnt/docs/dataset/sequences/00/image_1/%06d.png", frame_id);
  	Mat current_frame_rgb = imread(current_frame_path);
  	cvtColor(current_frame_rgb, current_frame_gray, COLOR_BGR2GRAY);
  	vector<uchar> keep_track;
  	FAST_Feature_Tracking(previous_frame, current_frame_gray, previous_frame_features, current_frame_features, keep_track);

  	Emat = findEssentialMat(current_frame_features, previous_frame_features, ff, pp, RANSAC, 0.999, 1.0, mask);
  	recoverPose(Emat, current_frame_features, previous_frame_features, Rmat, tvec, ff, pp, mask);

    Mat previous_points(2,previous_frame_features.size(), CV_64F);
    Mat current_points(2,current_frame_features.size(), CV_64F);


   for(int i=0;i<previous_frame_features.size();i++)	{   
   		previous_points.at<double>(0,i) = previous_frame_features.at(i).x;
  		previous_points.at<double>(1,i) = previous_frame_features.at(i).y;

  		current_points.at<double>(0,i) = current_frame_features.at(i).x;
  		current_points.at<double>(1,i) = current_frame_features.at(i).y;
    }

      // Update 
      Trans_ = Trans_ + scale*(Rot_*tvec);
      Rot_ = Rmat*Rot_;

    // Re compute FAST features if the number of tracked features falls below threshold
	  if (previous_frame_features.size() < 2000)	
    {
 		  FAST_Feature_Detection(previous_frame, previous_frame_features);
      FAST_Feature_Tracking(previous_frame,current_frame_gray,previous_frame_features,current_frame_features, keep_track);
 	  }

    previous_frame = current_frame_gray.clone();
    previous_frame_features = current_frame_features;

    int x = int(Trans_.at<double>(0)) + 200;
    int y = int(Trans_.at<double>(2)) + 150;
    circle(traced_trajectory, Point(x, y) ,1, CV_RGB(255,255,0), 2);
    rectangle( traced_trajectory, Point(10, 30), Point(550, 50), CV_RGB(0,0,0), CV_FILLED);
    imshow( "Visual Data", current_frame_rgb );
    imshow( "Traced Trajectory", traced_trajectory );

    waitKey(1);

  }

  clock_t stop = clock();
  double time_taken = double(stop - start) / CLOCKS_PER_SEC;
  cout << "Total time taken: " << time_taken << "s" << endl;

  return 0;
}
